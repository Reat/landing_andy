<?php
//informations necessaire pour la connexion à la BDD

$dbhost = 'getandy.fr.mysql';
$dbname = 'getandy_fr';

$dbuser = 'getandy_fr';
$dbpassword = 'ynroLzTQ';

// connexion
$dbh = new PDO(
    'mysql:host=' . $dbhost . ';dbname=' . $dbname, $dbuser, $dbpassword
);

// verfier qu'il existe ca permet de ne pas avoir le message au premier chargement de page
// Si nom existe

$errors = array();

if (isset($_POST['nom'])) {
    if (empty($_POST['nom'])) {
        $errors[] = 'champ nom vide';
        // si name > 50 chars
    } else if (mb_strlen($_POST['nom']) > 50) {
        $errors[] = 'champ nom trop long (50max)';
    }
}

//Si prenom existe

if (isset($_POST['prenom'])) {
    if (empty($_POST['prenom'])) {
        $errors[] = 'champ prenom vide';
        // si name > 50 chars
    } else if (mb_strlen($_POST['prenom']) > 50) {
        $errors[] = 'champ prenom trop long (50max)';
    }
}
// si email existe
if (isset($_POST['mail'])) {
    if (empty($_POST['mail'])) {
        $errors[] = 'champ email vide';
    } else if (mb_strlen($_POST['mail']) > 150) {
        $errors[] = 'champ email trop long (150max)';
        // filter_var
    } else if (!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'champ email non-valide';
    }
}

// si message existe et que le champ est non vide, on utilise trim() pour enlever les espaces/tabs en debut et fin de texte
if (isset($_POST['message']) && trim($_POST['message']) == "") {
    $errors[] = 'champ message vide';
}


// Insertion dans la base

if (isset($_POST['nom']) && count($errors) == 0) {
    //   die('nom:'.$_POST['nom'].'prenom:'.$_POST['prenom'].'mail:'.$_POST['mail'].'message:'.$_POST['message']);

    $sql = "insert into contact (nom, prenom, mail, message) values(:nom, :prenom, :mail, :message );";
    $sth = $dbh->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
    if($sth->execute(array(
        ':nom' => $_POST['nom'],
        ':prenom' => $_POST['prenom'],
        ':mail' => $_POST['mail'],
        ':message' => $_POST['message']
    ))){
        // send mail

        //then redirect
    };
}




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Get Andy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description"
          content="ANDY, l’application gratuite qui vous permet de lancer un tas d’activités ou défis à faire avec votre amoureux."/>
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="img/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="img/favicons/manifest.json">
    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/main.css">


</head>
<body>


<header id="header_container">
    <figure class="header_icon">
        <a href="#">
            <img src="img/burger_icon.svg" alt="button_scroll" id="burger_change">
        </a>
    </figure>
    <nav class="menu close">
        <figure class="close_menu">
            <a href="#">
                <img src="img/croix.svg" alt="button_scroll" id="close_change">
            </a>
        </figure>
        <ul class="fixednav">
            <li data-anchor="slide1" class="activeMenu"><a href="#firstPage">ACCUEIL</a></li>
            <li data-anchor="slide2"><a href="#secondPage">DÉCOUVRIR ANDY </a></li>
            <li data-anchor="slide3"><a href="#thirdPage">QUE FAIT ANDY ?</a></li>
            <li data-anchor="slide4"><a href="#fourPage">CONTACTEZ-NOUS</a></li>
        </ul>
        <section class="RS">
            <a href="https://twitter.com/appliandy" id="twitter" class="icon">

            </a>

            <a href="https://facebook.com/appliandy" id="facebook" class="icon">

            </a>

            <a href="https://instagram.com/appliandy" id="instagram" class="icon">

            </a>

            <a href="#" id="snapchat" class="icon">

            </a>
        </section>

        </ul>
        <div id="popup">
            <figure class="close_snapchat">
                <a href="#">
                    <img src="img/croix_white.svg" alt="croix_snapchat_andy">
                </a>
            </figure>
            <figure class="snap_code">
                <img src="img/qrcode.png" alt="qrcode_snapchat_andy">
            </figure>
        </div>
    </nav>
</header>

<div id="fullpage">

    <div class="section " id="section0">

        <header class="logo_container">
            <figure class="logo">
                <a>
                    <img src="img/logo.png" alt="logo_andy">
                </a>
            </figure>
        </header>
        <a class="main_sec0">

            <h1 class="title_1"><img src="img/andy_vec.svg">, la première application qui incite les couples <span></span>à décrocher
                du smartphone.</h1>
            <a href="#thirdPage"><input type="submit" value="Télécharger andy !" class="dl_buton first_btn" /></a>
            <h3 class="title_3">Qui est andy ?</h3>
            <figure class="scroll">
                <a href="#secondPage">
                    <img src="img/fleche_scroll.svg" alt="button_scroll">
                </a>
            </figure>

        </main>
    </div>
    <div class="section" id="section1">
        <main>

            <video id="video_andy">
                <source src="data/andy.mp4" type="video/mp4">
            </video>
            <figure class="play">
                <img src="img/play-video.svg" alt="bouton_play">
            </figure>
            <div id="cache_player"></div>
            <figure class="scroll">
                <a href="#thirdPage">
                    <img src="img/fleche_scroll.svg" alt="button_scroll">
                </a>
            </figure>
        </main>
    </div>


    <div class="section" id="section2">
        <main>
            <header class="logo_container">
                <figure class="logo logo_sec2" >
                    <img src="img/Logo_final.svg" alt="logo_andy" >
                </figure>
                <h1 class="h1_blue">Que fait andy ?</h1>
            </header>


            <section class="left_sec2">
                <a href="video.php"><input type="submit" value="Télécharger andy !" class="dl_buton dl_second"  /></a>
                <figure class="fig_iphone">
                    <img src="img/iphone2.png" alt="application_andy">
                </figure>
            </section>

            <section class="right_sec2">
                <figure class="icones_descr">
                    <img src="img/on-off.svg" alt="picto_buton">
                </figure>
                <article class="description_right">
                    <p>andy propose des activités sous forme de défis à faire en couple, afin de profiter pleinement des
                        bons moments avec son/sa partenaire.</p>
                </article>
                <figure class="icones_descr">
                    <img src="img/ampoule.svg" alt="picto_bulb">
                </figure>
                <article class="description_right">
                    <p>andy cherche à instaurer une nouvelle habitude dans la vie à deux : le retrait du smartphone dans
                        les moments partagés.</p>
                </article>
                <figure class="icones_descr">
                    <img src="img/coeur.svg" alt="picto_heart">
                </figure>
                <article class="description_right">
                    <p>andy c’est un accompagnement dans l’épanouissement du couple.</p>
                </article>
            </section>
            <div class="clear"></div>
            <figure class="scroll">
                <a href="#fourPage">
                    <img src="img/fleche_scroll_red.svg" alt="button_scroll">
                </a>
            </figure>
        </main>
    </div>
    <div class="section" id="section3">
        <main class="contact">
            <section class="titre_container">
                <h1 class="h1_blue">Une question ? Des avis ? <span></span>Ne faites pas les timides, contactez-nous !
                </h1>
            </section>
            <form method="post" action="core/mail.php" class="form">
                <div>
                    <?php

                    for ($i = 0; $i < count($errors); $i++) {
                        echo($errors[$i] . "<br />");
                    }
                    ?>
                </div>
                <input type="text" name="nom" class="nom" placeholder="Votre nom"/>
                <input type="text" name="prenom" class="prenom" placeholder="Votre prénom"/>
                <input type="text" name="mail" class="mail" placeholder="Votre adresse e-mail"/>
                <textarea name="message" class="message" placeholder="Votre message"></textarea>
                <input type="submit" value="ENVOYER" class="send_buton" />
            </form>
            <figure id="last_arrow">
                <a href="#firstPage" class="scroll_up">
                    <img src="img/fleche_scroll_red.svg" alt="button_scroll">
                </a>
            </figure>
        </main>
        <footer class="footer_sec3">
            <section class="footer_container">
                <ul class="nav_footer">
                    <li><a href="#firstPage">ACCUEIL</a></li>
                    <li><a href="#secondPage">DÉCOUVRIR ANDY</a></li>
                    <li><a href="#thirdPage">QUE FAIT ANDY ?</a></li>
                </ul>
                <section class="RS_footer">
                    <a href="https://twitter.com/appliandy" id="twitter_footer" class="icon"></a>

                    <a href="https://facebook.com/appliandy" id="facebook_footer" class="icon"></a>

                    <a href="https://instagram.com/appliandy" id="instagram_footer" class="icon"></a>

                    <a href="#" id="snapchat_footer" class="icon"></a>




                </section>
                <ul class="mention_footer">
                    <li>Mentions légales</li>
                    <li>Copyright</li>
                </ul>
            </section>
        </footer>
    </div>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/scrolloverflow.js"></script>
<script type="text/javascript" src="js/jquery.fullPage.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/video.js"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-88704815-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>
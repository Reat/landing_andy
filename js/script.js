
    $(document).ready(function () {
        $('#fullpage').fullpage({
            anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourPage'],



            // conditon classe active menu

            afterLoad: function (anchorLink, index) {

                if ($('body').hasClass('fp-viewing-firstPage') == true) {

                    $('.fixednav li').removeClass('activeMenu');
                    $('[data-anchor=slide1]').addClass('activeMenu');
                }
                else if ($('body').hasClass('fp-viewing-secondPage') == true) {

                    $('.fixednav li').removeClass('activeMenu');
                    $('[data-anchor=slide2]').addClass('activeMenu');
                }
                else if ($('body').hasClass('fp-viewing-thirdPage') == true) {

                    $('.fixednav li').removeClass('activeMenu');
                    $('[data-anchor=slide3]').addClass('activeMenu');
                }
                else if ($('body').hasClass('fp-viewing-fourPage') == true) {

                    $('.fixednav li').removeClass('activeMenu');
                    $('[data-anchor=slide4]').addClass('activeMenu');
                }


                // action player video


                if (index == 2) {

                    function displayVideo() {
                        $('#video_andy').show();
                    }

                    setTimeout(displayVideo, 50);


                    if ($('#video_andy')[0].currentTime === 0) {


                        function displayCache() {
                            $('#cache_player').show();
                        }



                        setTimeout(displayCache, 40);

                    }
                    function displayPlayButton() {
                        $('.play').show();
                    }
                    setTimeout(displayPlayButton, 40);



                }
                if( index === 1 ){
                    $('#burger_change').attr('src','img/burger_icon.svg')

                }
                if(index == 3){

                    $(".logo_sec2 > img").addClass('rotate360')
                }

            },
            onLeave: function (index) {

                if (index == 2) {

                    function hideVideo() {
                        $('#video_andy').hide();
                        $('#cache_player').hide();
                        $('.play').hide();
                    }

                    setTimeout(hideVideo, 900);

                    if ($('#video_andy').get(0).currentTime != 0 ){

                        $('.play').show();
                    }
                }
                if( index === 1 ){
                    $('#burger_change').attr('src','img/burger_icon_red.svg')

                }
                if(index == 3){
                    $(".logo_sec2 > img").removeClass('rotate360')
                }


            }

        });
    });




function menuOpen(){


    $(".header_icon").click(function () {


        if ($('.menu').hasClass('close') == true) {

            $('.menu').removeClass('close');
            $('.menu').addClass('open');
        }
        else {
            $('.menu').removeClass('open');
            $('.menu').addClass('close');
        }
    });
    $(".close_menu").click(function () {


        if ($('.close_menu').hasClass('close') == true) {

            $('.menu').removeClass('close');
            $('.menu').addClass('open');
        }
        else {
            $('.menu').removeClass('open');
            $('.menu').addClass('close');
        }
    });




}
menuOpen();


function animBurger(){

    var burger = $('#burger_change');
    var close = $('#close_change');

    burger.click(function () {

        $(burger).addClass("rotate_droite");
        $(close).removeClass("rotate_gauche");
    });
    close.click(function () {

        $(close).addClass("rotate_gauche");
        $(burger).removeClass("rotate_droite");
    });

}
animBurger();

function snapchat() {

    var snap = $('#snapchat');
    var snap2 = $('#snapchat_footer');
    var close = $('.close_snapchat');
    var box = $('#popup');

    snap.click(function () {

        box.css({"bottom" : "0px"})

    })
    close.click(function () {
        box.css({"bottom" : "-150px"})
    })

}
snapchat();
function snapchat2() {

        var snap = $('#snapchat_footer');
        var close = $('.close_snapchat');
        var box = $('#popup2');

        snap.click(function () {

            box.css({"bottom" : "3px"})

        })
        close.click(function () {
            box.css({"bottom" : "-180px"})
        })

}
snapchat2();

function telecharger(){

        $('.first_btn').click(function () {

            $('.dl_second').show();
            $('.fig_iphone').hide();
        })
}
telecharger();

























$( document ).ready(function() {

    function videoAction() {


        var playButton = $('.play') ;
        var videoAndy = $('#video_andy') ;
        var cachePlayer = $('#cache_player') ;


        videoAndy.click(function () {

            if (videoAndy.get(0).paused){

                videoAndy.get(0).play();
                playButton.hide();

            }

            else
            {

                playButton.show();
                videoAndy.get(0).pause();


            }

        });

        playButton.click(function () {


            cachePlayer.hide();
            playButton.hide();
            videoAndy.get(0).play();

        });

    }
    videoAction();

});